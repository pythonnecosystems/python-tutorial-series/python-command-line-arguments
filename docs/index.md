# Python 명령어 인수: sys.argv <sup>[1](#footnote_1)</sup>

<font size="3">Python 스크립트의 sys.argv 리스트를 사용하여 명령어 인수를 액세스하는 방법에 대해 알아본다.</font>

## 목차

1. [개요](./command-line-arguments.md#intro)
1. [명령어 인수란?](./command-line-arguments.md#sec_02)
1. [Python에서 sys.argv를 사용하는 방법](./command-line-arguments.md#sec_03)
1. [sys.argv의 실전 예](./command-line-arguments.md#sec_04)
1. [sys.argv를 사용한 오류와 예외 처리](./command-line-arguments.md#sec_05)
1. [요약](./command-line-arguments.md#summary)

<a name="footnote_1">1</a>: [Python Tutorial 34 — Python Command Line Arguments: Sys.argv](https://python.plainenglish.io/python-tutorial-34-python-command-line-arguments-sys-argv-6ea06320256b?sk=1cdb9fcaa5afddec15974a33de8d5e8f)를 편역하였습니다.
