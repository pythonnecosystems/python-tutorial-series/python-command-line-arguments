# Python 명령어 인수: sys.argv

## <a name="intro"></a> 개요
Python 명령어(command line) 인수에 대한 이 포스팅에서는 Python 스크립트에서 **sys.argv** 리스트를 사용하여 명령어 인수를 액세스하는 방법을 설명한다. 또한 **sys.argv**를 사용할 때 발생할 수 있는 오류와 예외를 처리하는 방법도 보인다.

그러나 첫째, 명령어 인수는 무엇이며 왜 유용할까?

## <a name="sec_02"></a> 명령어 인수란?
명령어 인수는 명령어를 실행할 때 Python 스크립트에 전달하는 값이다. 이들은 사용자의 입력 또는 선호도를 기반으로 스크립트의 동작을 사용자 정의하는 데 유용하다. 예를 들어 명령어 인수를 사용하여 입력 파일, 출력 형식, 장황성 수준(verbosity level) 또는 스크립트가 지원하는 다른 옵션을 지정할 수 있다.

명령어 인수는 **sys.argv**라는 특별한 리스트에 저장되며, 이는 `sys` 모듈의 일부이다. `sys` 모듈은 Python의 다양한 시스템 관련 함수와 변수에 대한 액세스를 제공한다. 다음과 같이 `import` 문을 사용하여 `sys` 모듈을 임포트할 수 있다.

```python
# Import the sys module
import sys
```

`sys.argv` 리스트는 스크립트의 이름을 첫 번째 요소로 포함하고 스크립트에 전달된 명령어 인수가 뒤따른다. `sys.argv` 리스트의 길이는 제공된 명령줄 인수의 수에 따라 다르다. Python의 다른 리스트와 마찬가지로 인덱싱 또는 슬라이싱을 사용하여 `sys.argv` 목록의 요소를 액세스할 수 있다.

예를 들어, 사용자에게 인사 메시지를 출력하는 `hello.py` 이라는 스크립트가 있다고 가정해보자. 이 스크립트는 하나의 명령어 인수를 사용하며, 이 인수는 사용자의 이름이다. 스크립트의 이름과 사용자의 이름을 각각 `sys.argv[0]`와 `sys.argv[1]`로 액세스할 수 있다. 다음은 `hello.py` 스크립트의 코드이다.

```python
# Import the sys module
import sys
# Print the name of the script
print("The name of the script is:", sys.argv[0])
# Print the name of the user
print("Hello,", sys.argv[1] + "!")
```

스크립트를 실행하려면 명령어 터미널을 열고 다음 명령을 입력해야 한다.

```bash
$ python hello.py Alice
```

위 스크립트의 출력은 다음과 같다.

```
The name of the script is: hello.py
Hello, Alice!
```

이 예에서 `sys.argv` 리스트에는 `sys.argv[0]`는 `'hello.py'`이고 `sys.argv[1]`은 `'Alice'`이다. 명령어 인수로 다른 이름을 전달할 수 있으며 스크립트는 그 이름을 지정한다.

이제 명령어 인수가 무엇인지 알고 `sys.argv`를 사용하여 이 인수를 액세스하는 방법을 알게 되었으므로 다양한 시나리오에서 이 인수를 사용하는 방법의 예를 살펴보겠다.

## <a name="sec_03"></a> Python에서 sys.argv를 사용하는 방법
이 절에서는 Python에서` sys.argv`를 사용하여 명령어 인수를 액세스하고 조작하는 방법을 설명한다. 스크립트를 보다 유연하고 사용자 친화적으로 만들기 위한 유용한 팁과 트릭도 알려 줄 것이다.

이전 절에서 살펴보았듯이 `sys.argv` 리스트에는 스크립트의 이름과 스크립트에 전달된 명령어 인수가 포함되어 있다. Python의 다른 리스트와 마찬가지로 인덱싱 또는 슬라이싱을 사용하여 `sys.argv` 리스트의 요소를 액세스할 수 있다. 예를 들어 스크립트의 이름인 `sys.argv[0]`를 사용하여 `sys.argv` 리스트의 첫 번째 요소를 출력할 수 있다. 또한 마지막으로 제공된 명령줄 인수인 `sys.argv[-1]`를 사용하여 `sys.argv` 리스트의 마지막 요소를 출력할 수 있다.

그러나 스크립트가 예상하는 명령어 인수의 정확한 수나 순서를 모르는 경우가 있다. 이 경우 `sys.argv` 리스트의 길이를 알기 위하여 `len()` 함수를 사용할 수 있다. 이는 스크립트에 전달된 명령어 인수의 갯수를 알려준다. `for` 루프를 사용하여 `sys.argv` 리스트의 요소를 반복하고 하나씩 처리할 수도 있다. 예를 들어 다음 코드를 사용하여 스크립트에 전달된 모든 명령어 인수를 출력할 수 있다.

```python
# Import the sys module
import sys
# Print the number of command line arguments
print("The number of command line arguments is:", len(sys.argv))
# Print each command line argument
for arg in sys.argv:
    print(arg)
```

다음 명령을 사용하여 스크립트를 실행하면,

```bash
$ python script.py foo bar baz
```

위 스크립트의 출력은 다음과 같다.

```
The number of command line arguments is: 4
script.py
foo
bar
baz
```

이 예에서 `len()` 함수는 `sys.argv` 리스트의 요소 갯수인 4를 반환한다. `for` 루프는 스크립트의 이름에서 시작하여 마지막 명령어 인수로 끝나는 `sys.argv` 리스트의 각 요소를 출력한다.

`len()` 함수와 `for` 루프를 사용하여 임의의 수의 명령줄 인수를 처리할 수 있는 스크립트를 작성할 수 있다. 그러나 때로는 스크립트를 사용하는 방법에 대해 사용자에게 더 많은 정보 또는 지침을 제공할 수 있다. 예를 들어 스크립트의 사용과 옵션을 설명하는 도움말 메시지를 표시하기를 원할 수 있다. 사용자가 명령어 인수의 갯수와 타입을 정확히 제공했는지 확인하고 제공하지 않은 경우 오류 또는 경고를 제기할 수 있다. 다음 절에서는 `sys.argv`의 오류와 예외를 처리하는 방법을 설명한다.

## <a name="sec_04"></a> sys.argv의 실전 예
이 절에서는 Python에서 `sys.argv`를 사용하여 유용한 대화형 스크립트를 만드는 예를 보인다. 파일 읽기와 쓰기, 계산 수행, 옵션 및 플래그 구문 분석 등 다양한 작업을 수행하는 데 `sys.argv`를 사용하는 방법을 설명한다.

파일을 읽고 쓸 때 `sys.argv`를 사용하는 간단한 예부터 살펴보자. 한 파일의 내용을 다른 파일로 복사하는 `copy.py` 스크립트가 있다고 가정하자. 스크립트는 소스 파일의 이름과 대상 파일의 이름이라는 두 가지 명령줄 인수를 사용한다. `sys.argv`를 사용하여 파일 이름을 액세스하고 `open()` 함수를 사용하여 파일을 읽고 쓸 수 있다. `copy.py` 스크립트의 코드는 다음과 같다.

```python
# Import the sys module
import sys
# Check if the user has provided two command line arguments
if len(sys.argv) == 3:
    # Get the names of the source and destination files
    source_file = sys.argv[1]
    destination_file = sys.argv[2]
    # Open the source file in read mode
    with open(source_file, "r") as source:
        # Open the destination file in write mode
        with open(destination_file, "w") as destination:
            # Read the contents of the source file
            contents = source.read()
            # Write the contents to the destination file
            destination.write(contents)
    # Print a success message
    print("Copied the contents of", source_file, "to", destination_file)
else:
    # Print an error message
    print("Usage: python copy.py source_file destination_file")
```

스크립트를 실행하려면 명령어 터미널을 열고 다음 명령을 입력한다

```bash
$ python copy.py hello.txt bye.txt
```

스크립트는 `hello.txt` 파일의 내용을 `bye.txt` 파일로 복사하고 성공 메시지를 출력한다. 명령어 인수를 2개 보다 많거나 그 미만으로 제공하는 경우 스크립트는 오류 메시지를 출력하고 스크립트의 정확한 사용법을 보여준다.

이 예에서 `sys.argv`를 이용하여 사용자가 복사하려는 파일 이름을 액세스하는 방법을 볼 수 있다. `len()` 함수를 사용하여 사용자가 올바른 수의 명령어 인수를 제공했는지 확인하고 제공하지 않은 경우 적절한 메시지를 출력하는 방법도 볼 수 있다.

계산을 수행하기 위해 `sys.argv`를 사용하는 또 다른 예를 보자. 두 수에 대한 기본적인 산술 연산을 수행하는 `calc.py` 이라는 스크립트가 있다고 가정하자. 스크립트는 첫 번째 수, 연산자 및 두 번째 수라는 세 가지 명령어 인수를 사용한다. `sys.argv`를 사용하여 수와 연산자에 접근하고 `eval()` 함수를 사용하여 식을 계산할 수 있다. 다음은 `calc.py` 스크립트의 코드이다.

```python
# Import the sys module
import sys
# Check if the user has provided three command line arguments
if len(sys.argv) == 4:
    # Get the first number, the operator, and the second number
    num1 = sys.argv[1]
    op = sys.argv[2]
    num2 = sys.argv[3]
    # Construct the expression
    expression = num1 + op + num2
    # Evaluate the expression
    result = eval(expression)
    # Print the result
    print(expression, "=", result)
else:
    # Print an error message
    print("Usage: python calc.py num1 op num2")
```

스크립트를 실행하려면 명령어 터미널을 열고 다음 명령을 입력한다

```bash
$ python calc.py 3 + 5
```

스크립트는 3과 5의 덧셈을 수행하고 그 결과를 출력할 것이다. -, *, / 또는 **와 같은 다른 유효한 연산자를 사용할 수 있으며 스크립트는 해당 연산을 수행한다. 명령어 인수를 3보다 많거나 그 미만으로 제공하면 스크립트는 오류 메시지를 출력하고 스크립트의 정확한 사용법을 보여준다.

이 예에서는 `sys.argv`를 사용하여 사용자가 사용하고자 하는 숫자와 연산자를 접근하는 방법을 볼 수 있다. 또한 `eval()` 함수를 사용하여 명령dj 인수로부터 구성된 식을 계산하는 방법도 볼 수 있다.

옵션과 플래그를 파싱하기 위해 `sys.argv`를 사용하는 예를 하나 더 살펴보자. 옵션과 플래그는 스크립트의 동작을 수정하는 특수 명령어 인수이다. 일반적으로 대시(`-`) 또는 이중 대시(`--`)로 시작하며 값 또는 부울 상태를 가질 수 있다. 예를 들어 일부 스크립트에서 `-h`, `--help`, `-v`, `--verbose`, `-o`, `--output` 등 같은 옵션과 플래그를 보았을 수 있다. 옵션과 플래그를 파싱하는 것은 다양한 경우와 시나리오를 처리해야 하기 때문에 까다로울 수 있다. 다행히 Python에는 옵션과 플래그를 파싱하는 과정을 단순화하는 `argparse`라는 모듈이 내장되어 있다. `argparse` 모듈을 사용하여 명령어 인수를 파싱하고 도움말 메시지, 오류 처리, 유형 검사, 기본값 등 같은 다양한 기능을 제공할 수 있는 파서 객체를 만들 수 있다.

사용자에게 인사 메시지를 출력하는 `greet.py`라는 스크립트가 있다고 가정하자. 스크립트는 사용자 이름인 위치 인수 하나를 취한다. 스크립트는 또한 `-l`, `--language` 및 `-c`, `--capitalize` 선택적 인수 둘을 취한다. `-l`, `-language` 옵션은 인사 메시지의 언어를 지정하며 기본값으로 `'en'`(영어)을 가진다. `-c`, `--capitalize` 옵션은 이름의 첫 글자를 대문자로 쓸지 여부를 지정하며 기본값으로 `False`를 가진다. `argparse` 모듈을 사용하여 이러한 인수를 구문 분석하고 도움말 메시지와 오류 처리를 제공할 수 있는 파서 객체를 만들 수 있다. 다음은 스크립트 `greet.py`의 코드이다.

```python
# Import the sys and argparse modules
import sys
import argparse


# Create a dictionary of greeting messages in different languages
greetings = {
    'en': 'Hello',
    'es': 'Hola',
    'fr': 'Bonjour',
    'de': 'Hallo',
    'it': 'Ciao',
    'pt': 'Olá',
    'zh': '你好',
    'ja': 'こんにちは',
    'hi': 'नमस्ते'
}

# Create a parser object
parser = argparse.ArgumentParser(description='Print a greeting message to the user.')
# Add a positional argument for the name of the user
parser.add_argument('name', type=str, help='the name of the user')
# Add an optional argument for the language of the greeting message
parser.add_argument('-l', '--language', type=str, default='en', help='the language of the greeting message (default: en)')
# Add an optional argument for the capitalization of the name
parser.add_argument('-c', '--capitalize', action='store_true', help='whether to capitalize the first letter of the name (default: False)')
# Parse the command line arguments
args = parser.parse_args()
# Get the name of the user
name = args.name
# Get the language of the greeting message
language = args.language
# Get the capitalization of the name
capitalize = args.capitalize
# Check if the language is valid
if language in greetings:
    # Get the greeting message in the specified language
    greeting = greetings[language]
else:
    # Print an error message and exit the script
    print("Invalid language:", language)
    sys.exit(1)
# Check if the capitalization is True
if capitalize:
    # Capitalize the first letter of the name
    name = name.capitalize()
# Print the greeting message and the name
print(greeting + ", " + name + "!")
```

스크립트를 실행하려면 명령어 터미널을 열고 다음 명령을 입력한다

```bash
$ python greet.py Alice -l fr -c
```

스크립트는 인사 메시지와 이름을 프랑스어로 인쇄하고 이름의 첫 글자를 대문자로 표시한다. 스크립트의 출력은 다음과 같다.

```
Bonjour, Alice!
```

다른 유효한 언어 코드를 사용할 수 있으며 스크립트는 해당 언어로 인사 메시지를 인쇄합니다. `-l`, `--language` 옵션을 생략할 수도 있으며, 이떄 스크립트는 기본값인 `'en'`(영어)을 사용한다.`-l`, `-language`를 생략할 수도 있다

## <a name="sec_05"></a> sys.argv를 사용한 오류와 예외 처리
이 절에서는 Python에서 `sys.argv`를 사용할 때 발생할 수 있는 오류와 예외 처리 방법에 대해 설명한다. 오류와 예외는 프로그램의 정상적인 흐름을 방해하고 프로그램을 종료시키거나 잘못 동작하게 만드는 예기치 않은 이벤트이다. python에서 오류와 예외를 탐지하고 처리하기 위해 `try`와 `exception` 문을 사용할 수 있다.

`sys.argv`를 사용할 때 발생할 수 있는 일반적인 오류 중 하나가 `IndexError`이다. 이 오류는 존재하지 않는 `sys.argv` 리스트의 요소를 액세스하려고 할 때 발생한다. 예를 들어 명령어 인수가 두 개 있을 것으로 예상되는 스크립트를 실행하지만 하나만 제공하는 경우 `sys.argv[2]`를 액세스하려고 할 때 `IndexError`가 발생한다. 이 오류를 방지하기 위해 `len()` 함수를 사용하여 사용자가 정확한 명령어 인수의 갯수를 제공했는지 확인하고 제공하지 않은 경우 오류 메시지 또는 도움말 메시지를 인쇄할 수 있다. 또한 `try`와 `except` 문을 사용하여 `IndexError`가 발생하는 경우 이를 잡아 처리할 수 있다. 예를 들어 이전 절에서 `hello.py` 스크립트를 다음과 같이 수정하여 `IndexError`를 처리할 수 있다.

```python
# Import the sys module
import sys
# Try to access the name of the user
try:
    # Get the name of the user
    name = sys.argv[1]
    # Print the name of the script
    print("The name of the script is:", sys.argv[0])
    # Print the name of the user
    print("Hello,", name + "!")
except IndexError:
    # Print an error message and a help message
    print("Error: Missing name argument")
    print("Usage: python hello.py name")
```

다음 명령을 사용하여 스크립트를 실행한다면,

```bash
$ python hello.py
```

이 스크립트는 `IndexError`를 잡아 오류 메시지와 도움말 메시지를 출력한다. 스크립트의 출력은 다음과 같다.

```
Error: Missing name argument
Usage: python hello.py name
```

이 예에서는 `sys.argv`를 사용할 때 발생할 수 있는 `IndexError`를 탐지하고 처리하기 위해 `try`와 `except` 문을 사용하는 방법을 볼 수 있다. 스크립트의 바른 사용법을 보여주는 유용한 메시지 출력을 볼 수 있다.

`sys.argv`를 사용할 때 발생할 수 있는 또 다른 일반적인 오류는 `ValueError`이다. 이 오류는 명령어 인수를 다른 데이터 타입으로 변환하려고 할 때 발생한다. 즉, 인수가 해당 데이터 타입과 호환되지 않는 경우이다. 예를 들어 숫자 명령어 인수를 예상하는 스크립트를 실행하지만 유효한 숫자가 아닌 문자열을 제공하면 인수를 정수 또는 실수로 변환하려고 할 때 `ValueError`가 표시된다. 이 오류를 방지하기 위해 `isnumeric()` 메서드를 사용하여 인수가 유효한지 확인하고 그렇지 않은 경우 오류 메시지 또는 도움말 메시지를 인쇄할 수 있다. 또한 `try`와 `except` 문을 사용하여 `ValueError`가 발생하면 이를 처리할 수 있다. 예를 들어 이전 절의 `calc.py` 스크립트를 다음과 같이 수정하여 `ValueError`를 처리할 수 있다.

```python
# Import the sys module
import sys
# Check if the user has provided three command line arguments
if len(sys.argv) == 4:
    # Get the first number, the operator, and the second number
    num1 = sys.argv[1]
    op = sys.argv[2]
    num2 = sys.argv[3]
    # Try to convert the numbers to floats
    try:
        # Convert the numbers to floats
        num1 = float(num1)
        num2 = float(num2)
        # Construct the expression
        expression = str(num1) + op + str(num2)
        # Evaluate the expression
        result = eval(expression)
        # Print the result
        print(expression, "=", result)
    except ValueError:
        # Print an error message and a help message
        print("Error: Invalid number argument")
        print("Usage: python calc.py num1 op num2")
else:
    # Print an error message and a help message
    print("Error: Missing or extra arguments")
    print("Usage: python calc.py num1 op num2")
```

다음 명령을 사용하여 스크립트를 실행한다면,

```bash
$ python calc.py 3 + five
```

이 스크립트는 `ValueError`를 확인하고 오류 메시지와 도움말 메시지를 출력한다. 이 스크립트의 출력은 다음과 같다.

```python
Error: Invalid number argument
Usage: python calc.py num1 op num2
```

위의 예에서 `sys.argv`를 사용할 때 발생할 수 있는 `ValueError`를 잡고 처리하기 위해 `try`와 `except` 문을 사용하는 방법을 볼 수 있다. 스크립트의 정확한 사용법을 보여주는 유용한 메시지 출력을 볼 수 있다.

이들은 Python에서 `sys.argv`를 사용하여 오류와 예외를 처리하는 예이다. `sys.argv`를 사용할 때 발생할 수 있는 다른 타입의 오류와 예외를 처리하는 데에도 동일한 기술을 사용할 수 있다. 또한 `argparse` 모듈을 사용하여 이전 절에서 살펴보았듯이 옵션과 플래그를 구문 분석하는 프로세스를 단순화할 수 있다. 오류와 예외를 `sys.argv`로 처리함으로써 스크립트를 보다 견고하고 사용자 친화적으로 만들 수 있다.

다음 절에서는 포스팅을 마무리하려고 한다.

## <a name="summary"></a> 요약
Python 명령어 인수에 대한 이 포스팅의 끝에 도달했다. 이 포스팅에서는 Python 스크립트에서 `sys.argv` 리스트를 사용하여 명령어 인수를 액세스하고 조작하는 방법을 설명하였다. 또한 `sys.argv`를 사용할 때 발생할 수 있는 오류와 예외를 처리하는 방법도 보였다. 파일 읽기와 쓰기, 계산 수행, 옵션과 플래그 구문 분석과 같은 다양한 작업을 수행하기 위해 `sys.argv`를 사용하는 방법의 예를 보였다. 옵션과 플래그 구문 분석 프로세스를 단순화하기 위해 `argparse` 모듈을 사용하는 방법도 설명하였다.

Python에서 `sys.argv`를 사용함으로써 스크립트를 보다 유연하고 사용자 친화적으로 만들 수 있다. 사용자의 입력이나 선호도를 기반으로 스크립트의 동작을 커스터마이즈할 수 있다. 스크립트 사용 방법에 대한 더 많은 정보나 지침을 사용자에게 제공할 수도 있다. `sys.argv`를 사용하여 Python 프로그래밍에서 다양한 문제를 해결할 수 있는 유용하고 인터렉티브한 스크립트를 만들 수 있다.
